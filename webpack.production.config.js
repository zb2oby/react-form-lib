const path = require('path');
module.exports = {
    mode: 'production',
    entry : './src/index.js',
    output: {
        path: path.join(__dirname, 'lib'),
        filename: 'index.js',
        libraryTarget: 'commonjs2',
    },
    externals: {
        react: {
            root: 'React',
            commonjs2: 'react'
        },
        "react-dom": "commonjs2 react-dom,"
    },
    module: {
        rules: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /\.scss$/,
                use: [ 'style-loader', 'css-loader', 'sass-loader' ]
            }
        ]
    },

};