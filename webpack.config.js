module.exports = (env) => {
    if (env.development) {
        process.env.NODE_ENV = 'development';
        return require(`./webpack.development.config.js`)
    }
    if (env.production) {
        process.env.NODE_ENV = 'production';
        return require(`./webpack.production.config.js`)
    }
}