import React, {useState} from 'react'
import {Form, FormItem, Select, RadioList, ErrorMsg} from "../src";
import "./App.scss";

const enumMrMrs = [
    {label: "Mr", value: "Mr"}, {label: "Mrs", value: "Mrs"}
];
const personTypes = [
    {label: "Parent1", value: "1"},
    {label: "Parent2", value: "2"}
];

function isNotEmpty(v) {
    return v !== undefined && v !== "" && v !== null && v.length !== 0 && v !== [];
}


// You can put these rules in external file and import it
/*All rules have to respect this format to be parsed by validation method in Form.
for each field you want to be validated you have to reference his exact "name" props in this array
then for each entry you have to put a "key" property (what you want : unique), a "message" property as an Error message to display after validation
and a "isValid" property that is a function which takes the value of the field and do whatever you want to return a boolean
You can put more than one rule per field reference.*/
const rules = {
    customGlobalRequiredMessage: "is empty. Please fill it.",
    maidenName: [
        {
            key: "maidenname_invalid",
            message: "This name is not allowed",
            isValid: (nextVal) => (nextVal !== "test")
        },
        {
            key: "maidenname_empty",
            message: "Maiden name is required if you are a girl. Please fill it.",
            isValid: (nextVal, values) => (values.title === '' || values.title === 'Mr' || (values.title === 'Mrs' && isNotEmpty(nextVal)))
        }
    ],
    lastName: [
        {
            key: "lastname_empty",
            message: "Last name can't contains numbers.",
                isValid: (nextVal) => (nextVal.match(/^[a-zA-Z]+$/))
        },

    ],
    firstName: [
        {
            key: "lastname_nonempty_but_firstname_empty",
            message: "FirstName has to contain more than 1 charachter.",
            isValid: (nextVal) => (nextVal.match(/^([A-Za-z]{2,})$/))
        }
    ],
};

const App = () => {

    const [persistOk, setPersistOk] = useState(false);
    const [apiErr, setApiErr] = useState(null);
    const [dataStore, setDataStore] = useState({});

    const persist = (data) => {

        //simulate api call
        let prom = new Promise((resolve, reject) => {
            // comment the resolve method call and uncomment the reject method call to simulate Service error and test ErrorMsg component
            resolve(setDataStore({data: data}))
            //reject("PERSISTENCE_MOCK_ERR_CODE")

        });

        return prom.then(() => {
            setPersistOk(true)
        })
        .catch((err) => {
            setApiErr(err)
            return Promise.reject(err)
        })
    };


    let customLabel = null;
    let customError = null;

    // uncomment these lines to test custom component for input label and input errors
    /*customLabel = (props) => <div style={{color: 'violet'}}>My Custom Label : {props && props.labelParams.text}</div>
    customError = (props) => <div style={{color: 'orange'}}>My Custom Error : {props && props.error}</div>*/


    const inputInfoExample = <label style={{display: 'block'}}>inputSubInfo</label>

    const myLabelWithGlobalParams = ({...myCustomParams}) => {
        return {...myCustomParams, inlinePosition:true, fixedWidth: 200}
    }

    return (
        <>
            <div className={`${persistOk ? 'filter-active' : ""} prf_container inline-block`}>
                {apiErr && <ErrorMsg error={`An error occured ! ${apiErr}`}/>}
                <Form validationRules={rules} customLabel={customLabel} customError={customError} >
                    <FormItem
                        name="person"
                        type={"select"}
                        labelParams={myLabelWithGlobalParams({text:"Person"})}
                        className={"prf_form-item"}
                    >
                        <Select dataSet={personTypes} placeHolder={"Select a type"} className={"prf_select prf_input"}/>
                    </FormItem>
                    <FormItem
                        name="title"
                        type={"radio"}
                        labelParams={myLabelWithGlobalParams({text:"Title civility"})}
                        className={"prf_form-item"}
                    >
                        <RadioList dataSet={enumMrMrs} />
                    </FormItem>
                    <div className={"prf-divider"}/>
                    <FormItem
                        name="maidenName"
                        type={"text"}
                        initialValue={"initial name"}
                        validationTrigger={'onChange'}
                        inputSubInfo={inputInfoExample}
                        labelParams={myLabelWithGlobalParams({text: "Maiden name", subLabel: "Name 'test' is not allowed"})}
                        className={"prf_form-item"}
                    >
                        <input className="prf_input" />
                    </FormItem>
                    <FormItem
                        name="lastName"
                        type={"text"}
                        required={true}
                        labelParams={myLabelWithGlobalParams({text: "Last name"})}
                        className={"prf_form-item"}
                    >
                        <input className="prf_input" />
                    </FormItem>
                    <FormItem
                        name="firstName"
                        type={"text"}
                        required={true}
                        labelParams={myLabelWithGlobalParams({text: "First name"})}
                        show={(values) => (values && values.lastName !== "")}
                        className={"prf_form-item"}
                    >
                        <input className="prf_input" />
                    </FormItem>
                    <div className={"prf-btn-toolbar"}>{/*Here example of wrapper for FormItem inside Form*/}
                        <FormItem
                            type={"submit"}
                            callBackAction={persist}
                            resetAfterSubmit={true} //if callBackAction Promise is resolved form will be reseted
                            className={"inline-block"}
                        >
                            <button className="prf-btn prf-btn--primary" type={"button"}>Submit</button>
                        </FormItem>
                        <FormItem
                            callBackAction={() => setApiErr(null)}
                            type={"reset"}
                            className={"inline-block"}
                        >
                            <button className="prf-btn prf-btn--primary" type={"button"}>Reset</button>
                        </FormItem>
                    </div>
                </Form>
                {persistOk && <div className={`modal`}><div className={"modal-message"}>Data saved correctly <pre>dataStore = {JSON.stringify(dataStore.data, null, 4)}</pre></div><button onClick={() => (setPersistOk(false))} className="prf-btn prf-btn--primary" type={"button"}>OK</button></div>}
            </div>
        </>
    )


}

export default App;
