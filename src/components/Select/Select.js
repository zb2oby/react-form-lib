import React from 'react';
import Option from "./Option";

/**
 * Encapsule une liste d'options
 * Props :
 * - dataSet : Tableau de valeurs {label: "", value: ""}
 * - name : nom du champ select
 * - onChange : callback appelé à la sélection d'une option, renvoie la valeur sélectionnée
 * - value : la valeur actuelle
 * - placeHolder : le texte inactif à afficher en premiere option
 */
function Select (props) {

    const { dataSet, name, className, onChange, value, placeHolder } = props;

    let elts = dataSet.map((v, k) => {
        return (
            <Option key={k} value={v} />
        )
    });
    if (placeHolder !== undefined && (value === undefined || value === "" || !dataSet.some(v => v.value === value.value))) {
        elts.unshift(<Option disabled key={"valuePH"} value={{label: placeHolder, value: ""}}/>)
    }



    return (
        <select name={name} value={value} className={className} onChange={(e) => onChange(e)}>
            {elts}
        </select>
    )

}

export default Select;