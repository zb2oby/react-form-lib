import React from 'react';

function Option (props) {

    const {value, disabled, selected} = props

    return (
        <>
            <option value={value.value} selected={selected} disabled={disabled}>{value.label}</option>
        </>

    )

}

export default Option;