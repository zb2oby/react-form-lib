import React from 'react'

function Label (props) {


    let style = {
        display: `${props.inlinePosition ? "inline-block" : "block"}`,
        verticalAlign: "middle"
    }

    if (props.fixedWidth) {
        style = {
            ...style,
            width: `${props.fixedWidth}px`,
        }
    }

    return(
        <div style={style} className={`prf-label`}>
            <label>{props.label}</label>{props.required && <span className={"prf_text--danger"}> *</span>}
            {props.sublabel && <p><small>{props.sublabel}</small></p>}
        </div>
    )
}

export default Label