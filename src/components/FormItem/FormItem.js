import {ErrorMsg, Label} from "../index";
import React from "react";

const FormItem = (props) => {

    let elt = null;

    let inputSubInfo = null;
    if (props.inputSubInfo) {
        inputSubInfo = <small style={{position: "absolute", bottom: "-17px", left: "3px"}}>{props.inputSubInfo}</small>
    }


    let LabelInput = null;
    if (props.customLabel) {
        let CustomLabel = props.customLabel
        LabelInput = <CustomLabel {...props}/>
    } else if (props.labelParams) {
        LabelInput = <Label label={props.labelParams.text} inlinePosition={props.labelParams.inlinePosition} fixedWidth={props.labelParams.fixedWidth} sublabel={props.labelParams.subLabel ? props.labelParams.subLabel : false} required={props.required ? props.required : false}/>
    }

    if (props.show) {
        elt =  (
            <div className={`${inputSubInfo ? "prf_subinfo" : ""}`}>
                {props.labelParams && props.labelParams.text && LabelInput}
                <div style={{display: `${props.labelParams && props.labelParams.inlinePosition ? "inline-block" : "block"}`, position: "relative"}}>
                    {props.children}
                    {inputSubInfo}
                </div>
            </div>)
    }

    let errors = null;
    if (props.errors && props.errors.length > 0) {
        errors = props.errors.map((error, i)=> {

            if (props.customError) {
                let CustomError = props.customError;
                return <CustomError {...props} error={error} key={i} />
            } else {
               return <ErrorMsg error={error} key={i} />
            }

        });

    }


    return(
        <div className={`${props.className ? props.className : ""}`}>
            {elt}
            {errors !== null && errors}
        </div>
    )
};

FormItem.displayName = "FormItem";
export default FormItem;