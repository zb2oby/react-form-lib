import React, {useState, useEffect} from 'react';


/**
 * Encapsule une liste de boutons radios
 * Props :
 * - dataSet : Tableau de valeurs {label: "", value: ""}
 * - name : nom des radios
 * - onChange : callback appelé à la sélection d'un bouton radio, renvoie la valeur sélectionnée
 * - value : la valeur actuelle
 * - defaultValue : un radio n'etans jamais seul on passe ici la valeur actuelle pour la seleciton par defaut
 */
function Radio (props) {

    const {value, checked, name, onChange} = props


    const id = 'radio_' + name + '_' + value.value;
    return (
        <>
            <label htmlFor={id}>{value.label}</label>
            <input type={"radio"} id={id} name={name} value={value.value} onChange={(e) => onChange(e)} checked={checked}/>
        </>

    )

}

export default Radio;