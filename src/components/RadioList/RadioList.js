import React from 'react'
import Radio from "./Radio";

/**
 * Encapsule une liste de boutons radios
 * Props :
 * - dataSet : Tableau de valeurs {label: "", value: ""}
 * - name : nom des radios
 * - onChange : callback appelé à la sélection d'un bouton radio, renvoie la valeur sélectionnée
 * - value : la valeur actuelle
 */
function RadioList (props) {

    const { dataSet, name, className, onChange, value } = props;

    let elts = dataSet.map((v, k) => {

        let checked = value === v.value

        return (
            <div key={k} className={'radio-item'}>
                <Radio value={v} onChange={(e) => onChange(e)} name={name} className={className} checked={checked}/>
            </div>
        )
    });

    return (
        <>
        {elts}
        </>
    )

};

export default RadioList;