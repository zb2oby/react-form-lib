import React from 'react'

function ErrorMsg (props) {
    return(
        <span className="prf_text--danger prf_text--error">
            <i className="icon prf_icon-erreur"/>
            {props.error}
        </span>
    )
}

export default ErrorMsg