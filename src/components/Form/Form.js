import React, {useEffect, useState} from 'react';

const Form = (props) => {

    const [items, setItems] = useState([]);
    const [values, setValues] = useState({});
    const [rules, setRules] = useState({});
    const [initialData, setInitialData] = useState({})
    const [elts, setElts] = useState([])

    useEffect(() => {
        setRules(props.validationRules);
    }, [])

    useEffect(() => {
        initItems();
    }, [rules])


    useEffect(() => {
        const itemsElt = loadFormItems(renderLogic)
        setElts(itemsElt);
    }, [items, props])
    

    const initItems = () => {
        let initialValues = {...initialData};

        const initLogic = (item) => {

            if (item.props.name) {
                if (item.props.initialValue) {
                    initialValues[item.props.name] = item.props.initialValue;

                } else {
                    initialValues[item.props.name] = ''

                }
            }

            let showProps = item.props.show;
            if (item.props.show === undefined) {
                showProps = () => true;
            }

            //callBack(initialValues);
            return React.cloneElement(item, {...item.props, show: showProps, key: `${item.props.name}_formItem`, initialValue: initialValues[item.props.name]});
        };
        let initialItems = loadFormItems(initLogic, props.children);
        setInitialData(initialValues);
        setValues(initialValues);
        setItems(initialItems);
        //reset();

    };


    const reset = () => {
        initItems()
    };


    const loadFormItems = (fn, children = items) => {
        let ch = [];
        if (children.length > 0) {
            ch = [...children]
        } else if (!Array.isArray(children)) {
            ch.push(children)
        }

        let returnChildren = ch.map((child, index) => {

            if (!React.isValidElement(child)) {
                return child;
            }


            if (child.type !== undefined && child.type.displayName && child.type.displayName === "FormItem") {
                child = fn(child)
            }
            if (child.props.children) {
                let newChildren = child.props.children;
                if (!Array.isArray(child.props.children)) {
                    newChildren = [child.props.children]
                }
                child = React.cloneElement(child, {
                    children: loadFormItems(fn, newChildren), key: index*2
                });
            }

            return child
        });

        if (returnChildren.length === 1) {
            return returnChildren[0]
        } else {
            return returnChildren;
        }

    };


    /**
     * Met à jour la valeur locale d'un element du formulaire
     * @param name
     * @param val
     */
    const onChange = (name, val) => {
        let callback = (vals) => setValues(vals);

        let localValues = {...values};
        const onChangeLogic = (item, value = val, callBack = callback) => {

            let itemProps = {...item.props}

            if (itemProps.name && itemProps.name !== "submit") {
                //pour l'item qu'on modifie

                if (itemProps.name === name) {


                    itemProps.errors = [];

                    if (itemProps.validationTrigger && itemProps.validationTrigger === "onChange") {
                        if (rules) {
                            //on charge la liste des regles du champ à valider
                            let itemRules = rules[itemProps.name];
                            //on itere sur les regle pour les champ actuellement visibles
                            if (itemRules) {
                                itemRules.map((rule) => {
                                    //si une règle n'est pas respectée on set isValid à false pour modifier le state et on charge les messages d'erreur dans l'item
                                    if (!rule.isValid(value, localValues) && (itemProps.show(localValues) === true)) {
                                        console.log(rule.message)
                                        itemProps.errors.push(rule.message);
                                    }
                                    return rule;
                                });

                            }
                        }
                    }


                    //cas de la remise à zero du champ au onchange
                    if (value === '' && itemProps.errors && itemProps.errors.length > 0) {
                        itemProps.errors = []
                    }


                    localValues[itemProps.name] = value;

                    if (itemProps.hasOwnProperty("callBackAction")) {
                        itemProps.callBackAction(localValues);
                    }

                } else if (!itemProps.show(localValues)) {

                    if (itemProps.errors && itemProps.errors.length > 0) {
                        itemProps.errors = [];
                    }                                              

                    if (initialData[itemProps.name]) {
                        localValues[itemProps.name] = initialData[itemProps.name];
                    }
                    else {
                        localValues[itemProps.name] = ""
                    }
                }
            }
            callBack(localValues);
            return React.cloneElement(item, {...itemProps})
        };


        let changedItems = loadFormItems(onChangeLogic)

        setValues(localValues);
        setItems(changedItems);
    };


    const isNotEmptyIfRequired = (v) => {
        return v !== undefined && v !== "" && v !== null && v.length !== 0 && v !== [];
    }
    

    /**
     * Valide l'ensemble des données du formulaire, met à jour les erreurs, et enregistre les données et renvoi l'etat
     * @returns {boolean}
     */
     const validateStep = (persistFunction, resetAfter) => {
        let isValid = true;

        let callback = (bool) => isValid = bool;

        const validateStepLogic = (item, callBack = callback) => {
            let props = {...item.props}
            if (props.name && props.name !== "submit") {
                //on vide les précédentes erreurs
                props.errors = [];

                //On vérifie d'abord que la donnée est bien saisie si elle est required.
                if (props.required && !isNotEmptyIfRequired(values[props.name], props.required) && (props.show(values) === true)) {
                    callBack(false);
                    // application du message required personnalisé
                    if (rules && rules.customGlobalRequiredMessage) {
                        props.errors.push(`${props.name} ${rules.customGlobalRequiredMessage}`);
                    } else {
                        props.errors.push(`${props.name} is required.`);
                    }

                } else {
                    //on charge la liste des regles du champ à valider
                    if (rules) {
                        let itemRules = rules[props.name];
                        //on itere sur les regle pour les champ actuellement visibles
                        if (itemRules) {
                            itemRules.map((rule) => {
                                //si une règle n'est pas respectée on set isValid à false pour modifier le state et on charge les messages d'erreur dans l'item
                                if (!rule.isValid(values[props.name], values) && (props.show(values) === true)) {
                                    callBack(false);
                                    props.errors.push(rule.message);
                                }
                                return rule;
                            });

                        }
                    }
                }


            }
            return React.cloneElement(item, {...props})
        }

        let validatedItems = loadFormItems(validateStepLogic);

        //si au moins une erreur a été rencontrée on recharge les items
        if (!isValid) {
            setItems(validatedItems);
        } else {
            let persist = new Promise(async resolve => resolve(persistFunction(values)));
            persist.then(() => {
                if (resetAfter) {
                    reset();
                }
            })

        }

        //on retourne la valeur de isValid pour l'etape suivante
        return isValid
    };


    const defineActionForEvent = (item) => {

        //let validationTrigger = item.props.validationTrigger;
        let inputName = item.props.name ? item.props.name : "";
        let inputType = item.props.type;
        let callBackAction = item.props.callBackAction;
        let actionProps = {};
        let action = null;
        let actionType = null;

        switch (inputType) {
            case "radio":
            case "select":
            case "text":
                actionType = "onChange";
                action = (e) => {
                    let val = e.target.value;
                    onChange(inputName, val);
                };
                break;
            case "reset":
                actionType = "onClick";
                action = () => {
                    reset();
                    if (callBackAction) {
                        callBackAction(initialData);
                    }
                };
                break;
            case "submit":
                actionType = "onClick";
                let resetAfter = item.props.resetAfterSubmit;
                action = () => validateStep(callBackAction, resetAfter);
                break;
            default:
                break;

        }
        actionProps[actionType] = action;
        return actionProps ;

    };


    const renderLogic = (item) => {
        let formItem = item;
        // Tous les FormItem concenrnant des inputs
        if (item.props.name) {

            //defninition des actions à passer à l'input en fonction des validationTrigger passés à formitem
            let actionProps = defineActionForEvent(item);

            //clonage de l'input avec les props du formitem

            let inputProps = {
                name: item.props.name,
                value: '',
            };


            if (values[item.props.name]) {
                inputProps.value = values[item.props.name];
            }


            let inputField = React.cloneElement(item.props.children, {...inputProps, ...actionProps, key: `${item.props.name}_input`})

            //clonage du formitem avec les nouveaux children et les nouvelles valeurs du state
            let localProps = {...item};
            if (props.customLabel) {
                localProps["customLabel"] = props.customLabel
            }

            if (props.customError) {
                localProps["customError"] = props.customError
            }

            localProps.show =  item.props.show !== undefined ? item.props.show(values) : () => true;
            localProps.key = item.props.name;
            formItem = React.cloneElement(item, {...localProps, errors: item.props.errors }, inputField);


        } else { // Tous les FormItem concenrnant autre chose (type bouton de validation ou autre)

            //clonage de l'input avec les props du formitem
            let actionProps = defineActionForEvent(item);

            let inputField = React.cloneElement(item.props.children, {...actionProps, key: `${item.props.name}_input`})

            //clonage du formitem avec les nouveaux children et les nouvelles valeurs du state
            let props = {...item};
            props.show = item.props.show !== undefined ? item.props.show(values) : () => true;
            formItem = React.cloneElement(item, {...props}, inputField);

        }

        return formItem;
    }


    return (
        <div>
            {elts}
        </div>

    )
    

}

export default Form;